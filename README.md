# Movie API

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

C# .NET project delivery for the assignment 3 module at Noroff. It reproduces the functionality requested in the assignment 3 file, as well as creating a web api in .Net core.

The main domain of the API are Movies. Movies belong to a certain franchise and have their corresponding characters, also known as actors. User are able to make CRUD Requests (GET, POST, PUT, DELETE) to manipulate data existing in SQL Server.

The functionality present allows users to get all movies, franchises and characters. Create new ones, as well as updating the existing data. If necessary, users are also able to delete certain database entries by sending DELETE requests with correct IDS. Additionally, users can query for all characters in a single movie, all movies belonging to a single franchise and all the characters present in a specific franchise.

If any of the web requests fail. For example, user queries for a single movie with an ID (queried as a parameter in the URL) that is not present in the database return correct status codes, in this case 404, as well as verbose error messages.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Dependencies](#dependencies)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [License](#license)

## Background
The background for this project was a mandatory assignment at Noroff Accelerate.

## Install
To install the project, simply clone the repository and open it up in Visual Studio.

#### Dependencies
.NET6.0
AutoMapper 11.0.1
AutoMapper Extensions Microsoft Dependency Injection
Microsoft Entity Framework
Swashbuckle

## Usage
In order to run the project, open up the project folder in Visual Studio and run MovieCharacterAPI.
To seed data, update the database with the update-database nuget packet manager console command.

## API

The api specification can be found by running the .Net core web api. Swagger spec will then open up in your default browser.

## Maintainers

[@Xerethars](https://github.com/Xerethars)

[@Magnus_Leira](https://github.com/h578031)

## License

MIT © 2022 Michal & Magnus
