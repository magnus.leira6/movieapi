﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.Exceptions;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Repository.CharacterRepo
{
    public class CrudCharacterRepoImpl : ICrudCharacterRepo
    {
        private readonly MovieDbContext _db;

        public CrudCharacterRepoImpl(MovieDbContext db)
        {
            _db = db;
        }

        public async Task<Character> CreateAsync(Character entity)
        {
            await _db.Characters.AddAsync(entity);         
            await _db.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteByIdAsync(int id)
        {
            Character? character = await _db.Characters.FindAsync(id);

            if (character == null) throw new EntityNotFoundException("Character has not been found");

            _db.Characters.Remove(character);
            await _db.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetAllAsync()
        {
            return await _db.Characters.ToListAsync();
        }

        public async Task<Character> GetByIdAsync(int id)
        {
            Character? character = await _db.Characters.FindAsync(id);
            if (character == null) throw new EntityNotFoundException("Character has not been found");

            return character;
        }

        public async Task<Character> UpdateAsync(int id, Character entity)
        {
            if (id != entity.Id) throw new RequestException("Character id is invalid");

            bool exists = await _db.Characters.AnyAsync(c => c.Id == id);
            if(!exists) throw new EntityNotFoundException("Character has not been found");

            _db.Entry(entity).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return entity;
        }
    }
}
