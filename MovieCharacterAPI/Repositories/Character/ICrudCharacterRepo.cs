﻿using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Repository.CharacterRepo
{
    public interface ICrudCharacterRepo : ICrudRepository<Character>
    {
    }
}
