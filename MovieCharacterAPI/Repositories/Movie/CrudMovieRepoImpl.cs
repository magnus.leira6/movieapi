﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.Exceptions;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repository.MovieRepo;

namespace MovieCharacterAPI.Repositories.MovieRepo
{
    public class CrudMovieRepoImpl : ICrudMovieRepo
    {
        private MovieDbContext _db;
        public CrudMovieRepoImpl(MovieDbContext db)
        {
            _db = db;

        }
        public async Task<Movie> CreateAsync(Movie entity)
        {
            await _db.AddAsync(entity);
            await _db.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteByIdAsync(int id)
        {
            Movie? movie = await _db.Movies.FindAsync(id);

            if (movie == null) throw new EntityNotFoundException("Movie has not been found");

            _db.Remove(movie);
            await _db.SaveChangesAsync();
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _db.Movies.ToListAsync();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            Movie? movie = await _db.Movies.FindAsync(id);

            if (movie == null) throw new EntityNotFoundException("Movie has not been found");

            return movie;
        }

        public async Task<List<Character>> GetMovieCharactersAsync(int id)
        {
            var movie = await _db.Movies.Where(m => m.Id == id)
                .Include(m => m.Characters)
                .FirstAsync();

            if (movie == null) throw new EntityNotFoundException("Movie with given id has not been found");

            return movie.Characters.ToList();
        }

        public async Task<Movie> UpdateAsync(int id, Movie entity)
        {
            if (id != entity.Id) throw new RequestException("Id and Movie Id do not match");

            bool exists = await _db.Movies.AnyAsync(m => m.Id == id);
            if (exists == false) throw new EntityNotFoundException("Movie with provided id does not exist");

            _db.Entry(entity).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return entity;
        }

        public async Task UpdateMovieCharactersAsync(int id, int[] characterIds)
        {
            Movie? movie = await _db.Movies.Where(m => m.Id == id)
                .Include(m => m.Characters)
                .FirstOrDefaultAsync();
            if (movie == null) throw new EntityNotFoundException("Movie has not been found");

            List<Character> newCharacters = new List<Character>();

            foreach (int charId in characterIds)
            {
                Character? character = await _db.Characters.Where(c => c.Id == charId).FirstOrDefaultAsync();
                if (character == null) throw new EntityNotFoundException("Character from list of character ids has not been found");
                newCharacters.Add(character);
            }

            movie.Characters = newCharacters;
            await _db.SaveChangesAsync();
        }
    }
}
