﻿using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Repository.MovieRepo
{
    public interface ICrudMovieRepo : ICrudRepository<Movie>
    {
        public Task<List<Character>> GetMovieCharactersAsync(int id);
        public Task UpdateMovieCharactersAsync(int id, int[] characterIds);
    }
}
