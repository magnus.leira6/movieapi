﻿using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Repository.FranchiseRepo
{
    public interface ICrudFranchiseRepo : ICrudRepository<Franchise>
    {
        public Task<ICollection<Movie>> GetFranchiseMovies(int id);
        public Task<ICollection<Character>> GetFranchiseCharacters(int id);
        public Task<Franchise> UpdateFranchiseMovies(int id, int[] movieIDs);
    }
}
