﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.Exceptions;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repository.FranchiseRepo;
using System.Linq;
using System.Reflection.Metadata.Ecma335;

namespace MovieCharacterAPI.Repositories.FranchiseRepo
{
    public class CrudFranchiseRepoImpl : ICrudFranchiseRepo
    {
        private readonly MovieDbContext _db;
        public CrudFranchiseRepoImpl(MovieDbContext db)
        {
            _db = db;
        }

        public async Task<Franchise> CreateAsync(Franchise entity)
        {
            await _db.AddAsync(entity);
            await _db.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteByIdAsync(int id)
        {
            Franchise? franchise = await _db.Franchises.Where(x => x.Id == id).FirstAsync();

            if (franchise == null) throw new EntityNotFoundException("Franchise has not been found");

            _db.Franchises.Remove(franchise);
            await _db.SaveChangesAsync();
        }

        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _db.Franchises.Include(x => x.Movies).ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            Franchise? franchise = await _db.Franchises.Where(x => x.Id == id).Include(x => x.Movies).FirstAsync();
            if (franchise == null) throw new EntityNotFoundException("Franchise has not been found");

            return franchise;
        }

        public async Task<ICollection<Character>> GetFranchiseCharacters(int id)
        {
            Franchise? franchise = await _db.Franchises.FindAsync(id);
            if(franchise == null) throw new EntityNotFoundException("Franchise has not been found");

            return await _db.Movies.Where(f => f.FranchiseId == id)
                .Include(m => m.Characters)
                .SelectMany(x => x.Characters).Distinct().ToListAsync();               
        }

        public async Task<ICollection<Movie>> GetFranchiseMovies(int id)
        {
            Franchise? franchise = await _db.Franchises.FindAsync(id);
            if (franchise == null) throw new EntityNotFoundException("Franchise has not been found");

            return await _db.Movies.Where(x => x.FranchiseId == id).ToListAsync();

        }

        public async Task<Franchise> UpdateAsync(int id, Franchise entity)
        {
            if (id != entity.Id) throw new RequestException("Franchise id is invalid");

            bool exists = await _db.Franchises.AnyAsync(x => x.Id == id);
            if (!exists) throw new EntityNotFoundException("Franchise has not been found");

            _db.Entry(entity).State = EntityState.Modified;
            await _db.SaveChangesAsync();
            return entity;
        }

        public async Task<Franchise> UpdateFranchiseMovies(int id, int[] movieIDs)
        {
            Franchise? franchise = await _db.Franchises.FindAsync(id);
            if (franchise == null) throw new EntityNotFoundException("Franchise has not been found");

            List<Movie> movies = movieIDs.ToList()
                .Select(mi => _db.Movies
                .Where(m => m.Id == mi).First())
                .ToList();

            franchise.Movies = movies;
            _db.Entry(franchise).State = EntityState.Modified;

            await _db.SaveChangesAsync();

            return franchise;

        }
    }
}
