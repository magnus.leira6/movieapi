﻿using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Services.CharacterService
{
    public interface ICrudCharacterService : ICrudService<Character>
    {
    }
}
