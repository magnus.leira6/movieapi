﻿using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repository.CharacterRepo;

namespace MovieCharacterAPI.Services.CharacterService
{
    public class CrudCharacterServiceImpl : ICrudCharacterService
    {
        private readonly ICrudCharacterRepo _repo;

        public CrudCharacterServiceImpl(ICrudCharacterRepo repo)
        {
            _repo = repo;
        }
        public async Task<Character> CreateAsync(Character entity)
        {
            return await _repo.CreateAsync(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _repo.DeleteByIdAsync(id);
        }

        public async Task<ICollection<Character>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }

        public async Task<Character> GetByIdAsync(int id)
        {
            return await _repo.GetByIdAsync(id);
        }

        public async Task<Character> UpdateAsync(int id, Character entity)
        {
            return await _repo.UpdateAsync(id, entity);
        }
    }
}
