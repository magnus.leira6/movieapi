﻿namespace MovieCharacterAPI.Services
{
    public interface ICrudService<T>
    {
        public Task<T> GetByIdAsync(int id);
        public Task<ICollection<T>> GetAllAsync();
        public Task<T> CreateAsync(T entity);
        public Task<T> UpdateAsync(int id, T entity);
        public Task DeleteByIdAsync(int id);
    }
}
