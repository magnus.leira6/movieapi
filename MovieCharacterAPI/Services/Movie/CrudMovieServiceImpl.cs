﻿using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repository.MovieRepo;

namespace MovieCharacterAPI.Services.MovieService
{
    public class CrudMovieServiceImpl : ICrudMovieService
    {
        private readonly ICrudMovieRepo _repo;

        public CrudMovieServiceImpl(ICrudMovieRepo repo)
        {
            _repo = repo;
        }
        public async Task<Movie> CreateAsync(Movie entity)
        {
            return await _repo.CreateAsync(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _repo.DeleteByIdAsync(id);
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            return await _repo.GetByIdAsync(id);
        }

        public async Task<List<Character>> GetMovieCharactersAsync(int id)
        {
            return await _repo.GetMovieCharactersAsync(id);
        }

        public async Task<Movie> UpdateAsync(int id, Movie entity)
        {
            return await _repo.UpdateAsync(id, entity);
        }

        public async Task UpdateMovieCharactersAsync(int id, int[] characterIds)
        {
            await _repo.UpdateMovieCharactersAsync(id, characterIds);
        }
    }
}
