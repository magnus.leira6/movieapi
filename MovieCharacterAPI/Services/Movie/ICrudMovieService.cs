﻿using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Services.MovieService
{
    public interface ICrudMovieService : ICrudService<Movie>
    {
        public Task<List<Character>> GetMovieCharactersAsync(int id);
        public Task UpdateMovieCharactersAsync(int id, int[] characterIds);
    }
}
