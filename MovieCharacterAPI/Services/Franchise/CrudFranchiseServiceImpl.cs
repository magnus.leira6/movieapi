﻿using MovieCharacterAPI.Models;
using MovieCharacterAPI.Repository.FranchiseRepo;

namespace MovieCharacterAPI.Services.FranchiseService
{
    public class CrudFranchiseServiceImpl : ICrudFranchiseService
    {
        private readonly ICrudFranchiseRepo _repo;

        public CrudFranchiseServiceImpl(ICrudFranchiseRepo repo)
        {
            _repo = repo;
        }

        public async Task<Franchise> CreateAsync(Franchise entity)
        {
            return await _repo.CreateAsync(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _repo.DeleteByIdAsync(id);
        }

        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            return await _repo.GetByIdAsync(id);
        }

        public async Task<ICollection<Character>> GetFranchiseCharacters(int id)
        {
            return await _repo.GetFranchiseCharacters(id);
        }

        public async Task<ICollection<Movie>> GetFranchiseMovies(int id)
        {
            return await _repo.GetFranchiseMovies(id);
        }

        public async Task<Franchise> UpdateAsync(int id, Franchise entity)
        {
            return await _repo.UpdateAsync(id, entity);
        }

        public async Task<Franchise> UpdateFranchiseMovies(int id, int[] movieIds)
        {
            return await _repo.UpdateFranchiseMovies(id, movieIds);
        }
    }
}
