﻿using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Services.FranchiseService
{
    public interface ICrudFranchiseService : ICrudService<Franchise>
    {
        public Task<ICollection<Movie>> GetFranchiseMovies(int id);
        public Task<ICollection<Character>> GetFranchiseCharacters(int id);
        public Task<Franchise> UpdateFranchiseMovies(int id, int[] movieIds);
    }
}
