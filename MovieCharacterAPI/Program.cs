using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MovieCharacterAPI.Context;
using MovieCharacterAPI.Repositories.FranchiseRepo;
using MovieCharacterAPI.Repositories.MovieRepo;
using MovieCharacterAPI.Repository.CharacterRepo;
using MovieCharacterAPI.Repository.FranchiseRepo;
using MovieCharacterAPI.Repository.MovieRepo;
using MovieCharacterAPI.Services.CharacterService;
using MovieCharacterAPI.Services.FranchiseService;
using MovieCharacterAPI.Services.MovieService;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

builder.Services.AddDbContext<MovieDbContext>(
    options => options.UseSqlServer(builder.Configuration.GetConnectionString("MovieDb")));


//AutoMapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle


var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Movie API",
        Description = "A Web API to manage movies, the franchise they belong to and their corresponding characters",
        Contact = new OpenApiContact
        {
            Name = "Magnus Leira & Michal Kowalski",
            Email = "magnus.leira@no.experis.com;michal.kowalski@no.experis.com?subject=Movies%20Api"
        }
    });
    options.IncludeXmlComments(xmlPath);
});

builder.Services.AddTransient<ICrudCharacterRepo, CrudCharacterRepoImpl>();
builder.Services.AddTransient<ICrudMovieRepo, CrudMovieRepoImpl>();
builder.Services.AddTransient<ICrudFranchiseRepo, CrudFranchiseRepoImpl>();

builder.Services.AddTransient<ICrudCharacterService, CrudCharacterServiceImpl>();
builder.Services.AddTransient<ICrudMovieService, CrudMovieServiceImpl>();
builder.Services.AddTransient<ICrudFranchiseService, CrudFranchiseServiceImpl>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
