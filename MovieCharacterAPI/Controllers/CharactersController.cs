﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.Exceptions;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Dtos.Characters;
using MovieCharacterAPI.Services.CharacterService;
using System.Net;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly ICrudCharacterService _service;
        private readonly IMapper _mapper;

        public CharactersController(ICrudCharacterService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all characters from database
        /// </summary>
        /// <response code="200">Returns all Characters</response>
        /// <returns>List og CharacterDto</returns>
        [HttpGet]
        public async Task<ActionResult<ICollection<CharacterDto>>> GetCharacters()
        {
            return Ok(
                _mapper.Map<List<CharacterDto>>(
                await _service.GetAllAsync()));
        }

        /// <summary>
        /// Get character with specific Id from database
        /// </summary>
        /// <response code = "200" > Returns the character</response>
        /// <response code="404">Id does not exist</response>
        /// <param name="id"></param>
        /// <returns>CharacterDto</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CharacterDto>> GetCharacter(int id)
        {
            try
            {
                return Ok(
                    _mapper.Map<CharacterDto>(
                    await _service.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Updates character with specific Id in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterDto"></param>
        /// <response code="200">Returns the updated character</response>
        /// <response code="400">Id does not match Character.Id</response>
        /// <response code="404">Id does not exist</response>
        /// <returns>CharacterDto</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CharacterDto>> PutCharacter(int id, CharacterPutDto characterDto)
        {
            try
            {
                Character character = _mapper.Map<Character>(characterDto);
                return Ok(
                    _mapper.Map<CharacterDto>(
                    await _service.UpdateAsync(id, character)));
            }
            catch (RequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Adds character to Database
        /// </summary>
        /// <param name="characterDto"></param>
        /// <response code="201">Returns the newly created character</response>
        /// <response code="400">The body is invalid</response>
        /// <returns>CharacterDto</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CharacterDto>> PostCharacter(CharacterPostDto characterDto)
        {
            Character character = _mapper.Map<Character>(characterDto);
            await _service.CreateAsync(character);
            return CreatedAtAction("GetCharacter", new { id = character.Id }, _mapper.Map<CharacterDto>(character));
        }

        /// <summary>
        /// Deletes character with specific Id from database
        /// </summary>
        /// <response code="204">The character was deleted</response>
        /// <response code="404">The Id does not exist</response>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            try
            {
                await _service.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = e.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                    );
            }
        }
    }
}
