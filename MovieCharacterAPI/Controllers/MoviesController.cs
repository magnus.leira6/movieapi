﻿using System.Net;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.Exceptions;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Dtos.Characters;
using MovieCharacterAPI.Models.Dtos.Movies;
using MovieCharacterAPI.Services.MovieService;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly ICrudMovieService _service;
        private readonly IMapper _mapper;

        public MoviesController(ICrudMovieService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all Movies from database
        /// </summary>
        /// <response code="200">Returns all Movies</response>
        /// <returns>List of MovieGetDto</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<MovieGetDto>>> GetMovies()
        {
            return Ok(_mapper.Map<List<MovieGetDto>>(await _service.GetAllAsync()));
        }

        /// <summary>
        /// Gets movie with specific Id from database
        /// </summary>
        /// <response code = "200" > Returns the movie</response>
        /// <response code="404">Id does not exist</response>
        /// <param name="id"></param>
        /// <returns>MovieGetDto</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MovieGetDto>> GetMovie(int id)
        {
            try
            {
                return Ok(_mapper.Map<MovieGetDto>( await _service.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Updates movie with specific Id in database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <response code="200">Returns the updated movie</response>
        /// <response code="400">Id does not match Movie.Id</response>
        /// <response code="404">Id does not exist</response>

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutMovie(int id, MoviePutDto movie)
        {
            try
            {
                Movie updatedMovie = await _service.UpdateAsync(id, _mapper.Map<Movie>(movie));
                return Ok(_mapper.Map<MovieGetDto>(updatedMovie));
            }
            catch (RequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Adds Movie to the database
        /// </summary>
        /// <response code="201">Returns the newly created movie</response>
        /// <response code="400">The body is invalid</response>
        /// <param name="dtoMovie"></param>
        /// <returns>MovieGetDto</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<MovieGetDto>> PostMovie(MoviePostDto dtoMovie)
        {
            Movie movie = _mapper.Map<Movie>(dtoMovie);
            movie = await _service.CreateAsync(movie);

            return CreatedAtAction("GetMovie", new { id = movie.Id }, _mapper.Map<MovieGetDto>(movie));
        }

        /// <summary>
        /// Deletes movie with specific Id from database
        /// </summary>
        /// <response code="204">The movie was deleted</response>
        /// <response code="404">The Id does not exist</response>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            try
            {
                await _service.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = e.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                    );
            }
        }
        /// <summary>
        /// Gets all characters from movie with specific Id
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Returns all characters in movie</response>
        /// <response code="404">The id was not found</response>
        /// <returns>List of CharacterDto</returns>
        [HttpGet("{id}/characters")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<CharacterDto>>> GetMovieCharacters(int id)
        {
            try
            {
                return Ok(_mapper.Map<List<Character>, List<CharacterDto>>( await _service.GetMovieCharactersAsync(id)));
            }
            catch(EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Updates the characters of movie with specific Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterIds"></param>
        /// <response code="204">The characters were updated for movie</response>
        /// <response code="404">Id was not found</response>
        /// <returns></returns>
        [HttpPut("{id}/characters")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateMovieCharacters(int id, int[] characterIds)
        {
            try
            {
                await _service.UpdateMovieCharactersAsync(id, characterIds);
                return NoContent();
            }
            catch(EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }
    }
}
