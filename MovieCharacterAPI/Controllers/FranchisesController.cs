﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.Exceptions;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Dtos.Characters;
using MovieCharacterAPI.Models.Dtos.Franchises;
using MovieCharacterAPI.Models.Dtos.Movies;
using MovieCharacterAPI.Services.FranchiseService;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly ICrudFranchiseService _service;
        private readonly IMapper _mapper;
        public FranchisesController(ICrudFranchiseService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the franchises in the database with related data as ids.
        /// </summary>
        /// <response code="200">Returns all franchises</response>
        /// <returns>List of FranchiseDto</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<FranchiseDto>>> GetFranchises()
        {
            return Ok(
                    _mapper.Map<List<FranchiseDto>>(
                        await _service.GetAllAsync()));
        }

        /// <summary>
        /// Gets the Franchise with a specific Id.
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Returns the franchise</response>
        /// <response code="404">Id does not exist</response>
        /// <returns>FranchiseDto</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FranchiseDto>> GetFranchise(int id)
        {
            try
            {
                return Ok(
                    _mapper.Map<FranchiseDto>(
                        await _service.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Updates the Franchise with specific Id in the database.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseDto"></param>
        /// <response code="200">Returns the updated franchise</response>
        /// <response code="400">Id does not match Franchise.Id</response>
        /// <response code="404">Id does not exist</response>
        /// <returns>FranchiseDto</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FranchiseDto>> PutFranchise(int id, FranchisePutDto franchiseDto)
        {
            try
            {
                Franchise franchise = _mapper.Map<Franchise>(franchiseDto);
                return Ok(
                    _mapper.Map<FranchiseDto>(
                    await _service.UpdateAsync(id, franchise)));
            }
            catch (RequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Adds a new Franchise to the database
        /// </summary>
        /// <response code="201">Returns the newly created franchise</response>
        /// <response code="400">The body is invalid</response>
        /// <param name="franchiseDto"></param>
        /// <returns>FranchiseDto</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<FranchiseDto>> PostFranchise(FranchisePostDto franchiseDto)
        {
            Franchise franchise = _mapper.Map<Franchise>(franchiseDto);
            await _service.CreateAsync(franchise);
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, _mapper.Map<FranchiseDto>(franchise));
        }

        /// <summary>
        /// Deletes the Franchise with specific Id from the database
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">The franchise was deleted</response>
        /// <response code="404">The Id does not exist</response>
        /// <returns>ActionResult</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            try
            {
                await _service.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = e.Message,
                        Status = (int)HttpStatusCode.NotFound
                    });
            }
        }

        /// <summary>
        /// Gets all Movies that are a part of the franchise
        /// </summary>
        /// <param name="id"></param>
        /// <response code="200">Returns the movies</response>
        /// <response code="404">The id does not exist</response>
        /// <returns>List of MovieGetDto</returns>
        [HttpGet("{id}/movies")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ICollection<MovieGetDto>>> GetFranchiseMovies(int id)
        {
            try
            {
                return Ok(
                    _mapper.Map<List<MovieGetDto>>(
                    await _service.GetFranchiseMovies(id)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Gets all Characters that play in a movie that is in the franchise
        /// </summary>
        /// <response code="200">Returns the characters</response>
        /// <response code="404">The id does not exist</response>
        /// <param name="id"></param>
        /// <returns>List of CharacterDto</returns>
        [HttpGet("{id}/characters")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetFranchiseCharacters(int id)
        {
            try
            {
                return Ok(
                    _mapper.Map<List<CharacterDto>>(
                    await _service.GetFranchiseCharacters(id)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Updates the movies that are in a specific franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIDs"></param>
        /// <response code="200">Returns the updated franchise</response>
        /// <response code="404">Id was not found</response>
        /// <returns>FranchiseDto</returns>
        [HttpPut("{id}/movies")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, int[] movieIDs)
        {
            try
            {
                return Ok(
                    _mapper.Map<FranchiseDto>(
                    await _service.UpdateFranchiseMovies(id, movieIDs)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }



    }
}
