﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Dtos.Franchises;

namespace MovieCharacterAPI.Profiles.Franchises
{
    public class FranchiseProfile: Profile
    {

        public FranchiseProfile()
        {
            CreateMap<FranchisePostDto, Franchise>();
            CreateMap<FranchisePutDto, Franchise>();

            CreateMap<Franchise, FranchiseDto>()
                .ForMember(dto => dto.Movies, opt =>
                opt.MapFrom(m => m.Movies.Select(i => i.Id).ToList()));

            
        }
        
    }
}
