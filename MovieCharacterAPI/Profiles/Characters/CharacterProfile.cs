﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Dtos.Characters;

namespace MovieCharacterAPI.Profiles.Characters
{
    public class CharacterProfile: Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDto>();

            CreateMap<CharacterPostDto, Character>();
            CreateMap<CharacterPutDto, Character>();
        }
        
    }
}
