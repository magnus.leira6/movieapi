﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Dtos.Movies;

namespace MovieCharacterAPI.Profiles.Movies
{
    public class MoviesProfile : Profile
    {
        public MoviesProfile()
        {
            CreateMap<MoviePostDto, Movie>();
            CreateMap<MoviePutDto, Movie>();
            CreateMap<Movie, MovieGetDto>();
        }
    }
}
