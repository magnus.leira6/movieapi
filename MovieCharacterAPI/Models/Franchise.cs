﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    public class Franchise
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Description { get; set; }

        //Navigation
        public ICollection<Movie>? Movies { get; set; }
    }
}
