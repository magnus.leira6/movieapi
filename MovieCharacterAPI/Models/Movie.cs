﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string Title { get; set; }
        [MaxLength (30)]
        public string Genre { get; set; }
        [Range(1900,3000)]
        public int Released { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        public string? PicURL { get; set; }
        public string? TrailerURL { get; set; }

        //Navigation
        public ICollection<Character>? Characters { get; set; }

        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
    }
}
