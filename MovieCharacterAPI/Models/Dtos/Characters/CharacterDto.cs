﻿namespace MovieCharacterAPI.Models.Dtos.Characters
{
    /// <summary>
    /// General DTO for the Character Class
    /// </summary>
    public class CharacterDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string? Alias { get; set; }
        public string? PictureURL { get; set; }
    }
}
