﻿namespace MovieCharacterAPI.Models.Dtos.Characters
{
    /// <summary>
    /// Character DTO used for updating Character entities.
    /// </summary>
    public class CharacterPutDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string? Alias { get; set; }
        public string? PictureURL { get; set; }
    }
}
