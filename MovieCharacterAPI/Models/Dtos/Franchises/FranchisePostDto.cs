﻿namespace MovieCharacterAPI.Models.Dtos.Franchises
{
    /// <summary>
    /// Franchise DTO for creating new Franchise entities
    /// </summary>
    public class FranchisePostDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
