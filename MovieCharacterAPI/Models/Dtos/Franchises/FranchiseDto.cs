﻿namespace MovieCharacterAPI.Models.Dtos.Franchises
{
    /// <summary>
    /// General Franchise DTO, which includes related movies represented with Id's
    /// </summary>
    public class FranchiseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> Movies { get; set; }
    }
}
