﻿namespace MovieCharacterAPI.Models.Dtos.Franchises
{
    /// <summary>
    /// Franchise DTO used for updating existing Franchise entities
    /// </summary>
    public class FranchisePutDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
