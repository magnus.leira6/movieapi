﻿namespace MovieCharacterAPI.Models.Dtos.Movies
{
    /// <summary>
    /// Movie DTO for updating an existing Movie entity
    /// </summary>
    public class MoviePutDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int Released { get; set; }
        public string Director { get; set; }
        public string? PicUrl { get; set; }
        public string? TrailerUrl { get; set; }
    }
}
