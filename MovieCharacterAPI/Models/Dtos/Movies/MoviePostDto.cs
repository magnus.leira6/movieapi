﻿namespace MovieCharacterAPI.Models.Dtos.Movies
{
    /// <summary>
    /// Movie DTO for creating a new Movie entity
    /// </summary>
    public class MoviePostDto
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public int Released { get; set; }

        public string? PicUrl { get; set; }
        public string? TrailerUrl { get; set; }
    }
}
