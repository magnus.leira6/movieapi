﻿namespace MovieCharacterAPI.Models.Dtos.Movies
{
    /// <summary>
    /// General Movie DTO used to represent Movie entities
    /// </summary>
    public class MovieGetDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public int Released { get; set; }
        public string? PicUrl { get; set; }
        public string? TrailerUrl { get; set; }
    }
}
