﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models
{
    public class Character
    {
        public int Id { get; set; }
        [MaxLength(30)]
        public string FirstName { get; set; }
        [MaxLength(40)]
        public string LastName { get; set; }
        [MaxLength(30)]
        public string? Alias { get; set; }
        [MaxLength(6)]
        public string Gender { get; set; }
        public string? PictureURL { get; set; }

        // Navigation
        public ICollection<Movie>? Movies { get; set; }
            
    }
}
