﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Context
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {          
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>()
                .HasData(
                    new Franchise { Id = 1, Name = "Lord of the Rings", Description = "Many rings, few fingers" },
                    new Franchise { Id = 2, Name = "Harry Potter", Description = "Kid with magic stick" },
                    new Franchise { Id = 3, Name = "Teletubies", Description = "Furries with antennas on top of their head" }
                );

            modelBuilder.Entity<Franchise>()
                .HasMany(f => f.Movies)
                .WithOne(m => m.Franchise)
                .HasForeignKey(m => m.FranchiseId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Movie>()
                .HasData(
                    new Movie { Id = 1, Title = "Two Towers", Director = "Peter Jackson", Genre = "Fantasy", Released = 2002, FranchiseId = 1 },
                    new Movie { Id = 2, Title = "Batman", Director = "Christopher Nolan", Genre = "Action", Released = 2010, FranchiseId = 3 },
                    new Movie { Id = 3, Title = "Kill Bill", Director = "Tarantino", Genre = "Action", Released = 2002, FranchiseId = 2 }
                ) ;

            modelBuilder.Entity<Character>()
               .HasData(
                   new Character { Id = 1, FirstName="Ian", LastName="McKellen", Gender="Man"},
                   new Character { Id = 2, FirstName = "Christian", LastName = "Bale", Gender = "Man" },
                   new Character { Id = 3, FirstName = "Elijah", LastName = "Wood", Gender = "Man" }
               );

            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 3 }
                            );
                    });
        }
    }

    
}
